from time import sleep

import pytest
from signers import Signer


def test_success_pyjwt_signer():
    signer = Signer(secret="8h293j90d", salt="h328dh9320d9j230")
    start_payload = {"a": 1, "c": 3}
    encoded_data = signer.sign(payload=start_payload, action="encode", using="pyjwt")
    assert (
        signer.sign(payload=encoded_data, action="decode", using="pyjwt")
        == start_payload
    )


def test_expired_jwt_signer():
    signer = Signer(secret="8h293j90d", salt="h328dh9320d9j230")
    start_payload = {"a": 1, "c": 3}
    encoded_data = signer.sign(
        payload=start_payload, action="encode", using="pyjwt", expiration=5
    )
    sleep(6)
    with pytest.raises(ValueError):
        signer.sign(payload=encoded_data, action="decode", using="pyjwt")


def test_success_itsdangerous_signer():
    signer = Signer(secret="8h293j90d", salt="h328dh9320d9j230")
    start_payload = {"a": 1, "c": 3}
    encoded_data = signer.sign(
        payload=start_payload, action="encode", using="itsdangerous"
    )
    assert (
        signer.sign(payload=encoded_data, action="decode", using="itsdangerous")
        == start_payload
    )


def test_expired_itsdangerous_signer():
    signer = Signer(secret="8h293j90d", salt="h328dh9320d9j230")
    start_payload = {"a": 1, "c": 3}

    signer.sign(payload=start_payload, action="encode", using="itsdangerous")
    sleep(6)
    with pytest.raises(ValueError):
        signer.sign(
            payload="ufwefhwef0w9e0f",
            action="decode",
            using="itsdangerous",
            expiration=5,
        )


def test_failed_itsdangerous_signer():
    signer = Signer(secret="8h293j90d", salt="h328dh9320d9j230")
    start_payload = {"a": 1, "c": 3}
    with pytest.raises(NotImplementedError):
        signer.sign(payload=start_payload, action="encode", using="any_method")
    with pytest.raises(NotImplementedError):
        signer.sign(payload="ufwefhwef0w9e0f", action="decode", using="any_method")
