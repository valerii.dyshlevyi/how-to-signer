#!venv/bin/python
import json
from argparse import ArgumentParser
from pathlib import Path
from signers import Signer


def get_parser():
    arg_parser = ArgumentParser()
    arg_parser.add_argument("--secret", help="sets secret", required=True)
    arg_parser.add_argument("--salt", help="sets salt", required=True)
    arg_parser.add_argument(
        "--action",
        help="sets action use only decode or encode",
        choices=("encode", "decode"),
        required=True,
    )
    arg_parser.add_argument(
        "--using",
        help="sets using use only pyjwt or itsdangerous",
        choices=("pyjwt", "itsdangerous"),
        required=True,
    )
    arg_parser.add_argument(
        "--expiration", help="sets expire time in seconds", type=int
    )
    arg_parser.add_argument("--input", help="sets input file use *.json", required=True)
    arg_parser.add_argument(
        "--output", help="sets output file use *.json", required=True
    )

    return arg_parser


if __name__ == "__main__":
    # Parse arguments
    args = get_parser().parse_args()
    input_path = Path(args.input)
    if not input_path.exists():
        raise ValueError("Input file doesn't exists")
    with open(args.input, "r") as input_file, open(args.output, "w") as output_file:
        # Try to load file
        try:
            input_data = json.load(input_file)
        except TypeError:
            raise ValueError("Input file is broken. Time to go home!")

        # Is list inside
        if not isinstance(input_data, list):
            raise ValueError("You are not list!")

        if args.action == "encode" and any(not isinstance(i, dict) for i in input_data):
            raise ValueError("Not a dict!")
        elif args.action == "decode" and any(
            not isinstance(i, str) for i in input_data
        ):
            raise ValueError("Not a str!")
        signer = Signer(
            secret=args.secret,
            salt=args.salt,
        )

        output_file.write(
            json.dumps(
                [
                    signer.sign(
                        payload=i,
                        action=args.action,
                        using=args.using,
                        expiration=args.expiration,
                    )
                    for i in input_data
                ]
            )
        )
