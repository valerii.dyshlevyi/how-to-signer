import itsdangerous
import jwt
from datetime import datetime, timedelta
from typing import Union


class Signer:
    def __init__(self, secret: str, salt: str):
        self._salt: str = salt
        self._secret: str = secret

    def sign(
        self,
        payload: Union[dict, str],
        action: str = "encode",
        using: str = "pyjwt",
        expiration: int = 0,
    ):
        method_name = f"_{action}_by_{using}"
        if not hasattr(self, method_name):
            raise NotImplementedError(f"{method_name} is not implemented.")
        return getattr(self, method_name)(
            **{"payload": payload, "expiration": expiration}
        )

    def _encode_by_pyjwt(self, *args, **kwargs):
        expiration = kwargs.get("expiration")
        payload = kwargs.get("payload")
        if expiration:
            payload.update(
                {"exp": (datetime.now() + timedelta(seconds=expiration)).timestamp()}
            )
        return jwt.encode(payload=payload, key=self._secret, algorithm="HS256")

    def _decode_by_pyjwt(self, *args, **kwargs):
        return jwt.decode(
            jwt=kwargs.get("payload"), key=self._secret, algorithms=["HS256"]
        )

    def _encode_by_itsdangerous(self, *args, **kwargs):
        signer = itsdangerous.URLSafeTimedSerializer(secret_key=self._secret)
        return signer.dumps(obj=kwargs.get("payload"), salt=self._salt)

    def _decode_by_itsdangerous(self, *args, **kwargs):
        signer = itsdangerous.URLSafeTimedSerializer(secret_key=self._secret)
        expiration = kwargs.get("expiration") or 60
        try:
            return signer.loads(
                s=kwargs.get("payload"), max_age=expiration, salt=self._salt
            )
        except itsdangerous.BadSignature:
            raise ValueError("It's too late apologize...")
